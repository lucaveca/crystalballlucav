package com.example.student.myapplication;

/**
 * Created by Student on 8/26/2015.
 */
public class Predictions {

    public static Predictions predictions;
    private String[] answers;

    private Predictions() {
        answers = new String[] {
                "Your wishes will come true.",
                "Your wishes will never come true.",
                "hello",
                "goodbye"
        };
    }

    public static Predictions get() {
        if(predictions == null) {
            predictions = new Predictions();
        }
        return predictions;
    }

    public String getPrediction() {
        int range = (answers.length - 1);
        int rand = (int)(Math.random() * range) + 1;
        return answers[rand];
    }
}
